// Test isTree

let leaf = new Node('', 0, 0);

console.assert(isTree(leaf), 'A node with no children is a tree.');

let root = new Node('', 0, 0);
root.connectTo(leaf);

console.assert(isTree(root), 'A node with one child is a tree.');

leaf.connectTo(root);

console.assert(! isTree(root), 'Two nodes that have each other as children are not a tree.');

//     a
//    / \
//   b   c
//    \ /
//     d

let a = new Node('', 0, 0);
let b = new Node('', 0, 0);
let c = new Node('', 0, 0);
let d = new Node('', 0, 0);

a.connectTo(b);
a.connectTo(c);
b.connectTo(d);
c.connectTo(d);

console.assert(! isTree(a) && isTree(b) && isTree(c) && isTree(d), 'A graph of the structure depicted in the comment above is not a tree.');
