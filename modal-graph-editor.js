/*
 * Global state
 */

const CANVAS = document.getElementById('canvas');
const CTX = CANVAS.getContext('2d');
const GAP_SIZE = 10;
let NODES = [];
let MODE = 'normal';
let NODE_POINTER = -1;
let SELECTED_NODE;
let SKIP_INSERT = false;
let ACTIVE_COMMAND = null;
let CURRENT_PROJECT_NAME;
let EXISTING_PROJECTS = getExistingProjects();
let BUFFER = '';
let NODE_TO_CONNECT = null;
let IS_FULLSCREEN = false;
let IS_EXPORT_VIEW = false;

/** Stores the x coordinate when the mouse starts to drag the canvas to pan it. */
let DRAG_START_X;

/** Stores the y coordinate when the mouse starts to drag the canvas to pan it. */
let DRAG_START_Y;

/*
 * Classes
 */

class Node {
    constructor(text, x, y, id) {
        const textInfo = CTX.measureText(text);
        this.id = id || genRandomId();
        this.text = text;
        this.width = textInfo.width;
        this.height = 16;
        this.x = x;
        this.y = y;
        this.children = [];
        this.highlight = false;
    }

    setText(newText) {
        this.text = newText;
        this.width = CTX.measureText(newText).width;
    }

    connectTo(node) {
        this.children.push(node);
    }
}

/*
 * Execution
 */

CANVAS.addEventListener('mousedown', evt => {
    // Only allow panning with the main mouse button.
    if (evt.buttons !== 1) return;
    DRAG_START_X = evt.x;
    DRAG_START_Y = evt.y;
});

CANVAS.addEventListener('mousemove', evt => {
    // Only allow panning with the main mouse button.
    if (evt.buttons !== 1) return;
    panGraph(evt.x - DRAG_START_X, evt.y - DRAG_START_Y);
    renderNodes(CANVAS, CTX);
    DRAG_START_X = evt.x;
    DRAG_START_Y = evt.y;
});

window.addEventListener('keydown', handleKeyPress);

document.getElementById('delete-button').addEventListener('click', evt => {
    resetProjectState();
    localStorage.removeItem(CURRENT_PROJECT_NAME);
    setMessage(`Deleted project '${CURRENT_PROJECT_NAME}'`);
    renderNodes(CANVAS, CTX);
    evt.preventDefault();
});
document
    .getElementById('fullscreen-button')
    .addEventListener('click', evt => {
        if (IS_FULLSCREEN) {
            restoreFromFullscreen();
        } else {
            makeFullscreen();
        }
        IS_FULLSCREEN = !IS_FULLSCREEN;
        evt.preventDefault();
    });
document
    .getElementById('open-button')
    .addEventListener('click', evt => {
        openProject(CURRENT_PROJECT_NAME);
        evt.preventDefault();
    });
document
    .getElementById('save-button')
    .addEventListener('click', evt => {
        saveProjectAs(CURRENT_PROJECT_NAME);
        evt.preventDefault();
    });
document
    .getElementById('new-button')
    .addEventListener('click', evt => {
        newProject();
        evt.preventDefault();
    });
document
    .getElementById('export-button')
    .addEventListener('click', (evt) => {
        exportToImage();
        evt.preventDefault();
    });

updateCanvasDimensions();

/**
 * Used to store the canvas width when it's not fullscreen so we can restore to
 * this value.
 */
const CANVAS_WIDTH = CANVAS.width;

/**
 * Used to store the canvas height when it's not fullscreen so we can restore to
 * this value.
 */
const CANVAS_HEIGHT = CANVAS.height;

CTX.font = '20px Helvetica';
CTX.fillStyle = '#333';

let myNode = new Node('Root', 10, 50);
let myNewNode = new Node('Leaf', 100, 100);
myNode.connectTo(myNewNode);
NODES.push(myNode);
NODES.push(myNewNode);
NODE_POINTER = 0;
setSelectedNode(myNode);

// Focus on the canvas so the command will work
CANVAS.focus();

renderNodes(CANVAS, CTX);

/*
 * Functions
 */

function moveToCentreOfCanvas(node) {
    moveGraphTo(
        Math.floor(CANVAS.width / 2),
        Math.floor(CANVAS.height / 2),
        node
    );
}

function moveToTopCentreOfCanvas(node) {
    moveGraphTo(Math.floor(CANVAS.width / 2), 50, node);
}

function handleKeyPress(evt) {
    switch (evt.key) {
        case 'Escape':
            MODE = 'normal';
            setActiveCommand(null);
            BUFFER = '';
            NODES = NODES.map(n => {
                n.highlight = false;
                return n;
            }); // This is inefficient, FIXME
            NODE_TO_CONNECT = null;
            break;
        case 'v':
            if (MODE === 'normal') NODE_TO_CONNECT = SELECTED_NODE;
            break;
        case 'c':
            if (MODE === 'normal') {
                if (!SELECTED_NODE) {
                    setMessage(`No node selected`);
                } else if (!NODE_TO_CONNECT) {
                    setMessage(`No node designated to connect to`);
                } else {
                    NODE_TO_CONNECT.connectTo(SELECTED_NODE);
                    NODE_TO_CONNECT = null;
                }
            }
            break;
        case 'C':
            if (MODE === 'normal') {
                if (!SELECTED_NODE) {
                    setMessage(`No node selected`);
                } else if (!NODE_TO_CONNECT) {
                    setMessage(`No node designated to disconnect from`);
                } else {
                    NODE_TO_CONNECT.children = NODE_TO_CONNECT.children.filter(
                        n => n.id !== SELECTED_NODE.id
                    );
                    NODE_TO_CONNECT = null;
                }
            }
            break;
        case ':':
            if (MODE === 'normal') {
                MODE = 'buffer-text';
                setActiveCommand(':');
            }
            break;
        case '/':
            if (MODE === 'normal') {
                MODE = 'buffer-text';
                setActiveCommand('/');
            }
            break;
        case 'x':
            if (MODE === 'normal' && SELECTED_NODE) {
                deleteReferencesTo(SELECTED_NODE.id);
                deleteNode(SELECTED_NODE.id);
            }
            break;
        case 'g':
            if (MODE === 'normal') {
                if (ACTIVE_COMMAND === 'g') {
                    NODE_POINTER = 0;
                    setSelectedNode(NODES[0]);
                    setActiveCommand(null);
                } else {
                    setActiveCommand('g');
                }
            }
            break;
        case 'z':
            if (MODE === 'normal') {
                if (ACTIVE_COMMAND === 'z' && SELECTED_NODE) {
                    moveToCentreOfCanvas(SELECTED_NODE);
                    setActiveCommand(null);
                } else {
                    setActiveCommand('z');
                }
            }
            break;
        case 't':
            if (MODE === 'normal') {
                if (ACTIVE_COMMAND === 'z' && SELECTED_NODE) {
                    moveToTopCentreOfCanvas(SELECTED_NODE);
                    setActiveCommand(null);
                } else {
                    setActiveCommand('z');
                }
            }
        case 'H':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    mapOverChildren(R.curry(moveNode)(-5, 0), SELECTED_NODE);
                }
            }
            break;
        case 'J':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    mapOverChildren(R.curry(moveNode)(0, 5), SELECTED_NODE);
                }
            }
            break;
        case 'K':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    mapOverChildren(R.curry(moveNode)(0, -5), SELECTED_NODE);
                }
            }
            break;
        case 'L':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    mapOverChildren(R.curry(moveNode)(5, 0), SELECTED_NODE);
                }
            }
            break;
        case 'h':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    SELECTED_NODE.x -= 5;
                }
            }
            break;
        case 'j':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    SELECTED_NODE.y += 5;
                }
            }
            break;
        case 'k':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    SELECTED_NODE.y -= 5;
                }
            }
            break;
        case 'l':
            if (MODE === 'normal') {
                if (SELECTED_NODE) {
                    SELECTED_NODE.x += 5;
                }
            }
            break;
        case 'o':
            if (MODE === 'normal') {
                let newNode;
                if (SELECTED_NODE) {
                    newNode = new Node(
                        '',
                        SELECTED_NODE.x,
                        SELECTED_NODE.y + 50
                    );
                    SELECTED_NODE.children.push(newNode);
                } else {
                    newNode = new Node(
                        '',
                        Math.floor(CANVAS.width / 2),
                        Math.floor(CANVAS.height / 2)
                    );
                }
                NODE_POINTER = NODES.push(newNode) - 1;
                setSelectedNode(newNode);
                MODE = 'insert';
                SKIP_INSERT = true;
            }
            break;
        case 'A':
            if (MODE === 'normal') {
                MODE = 'insert';
                SKIP_INSERT = true;
            }
            break;
        case 'w':
            if (MODE === 'normal') {
                NODE_POINTER++;
                if (NODE_POINTER >= NODES.length) NODE_POINTER = 0;
                setSelectedNode(NODES[NODE_POINTER]);
            }
            break;
        case 'n':
            if (MODE === 'normal') {
                getHighlightedNode(R.add(1));
            }
            break;
        case 'N':
            if (MODE === 'normal') {
                getHighlightedNode(R.subtract(1));
            }
            break;
        case 'Enter':
            if (MODE === 'buffer-text') {
                switch (BUFFER) {
                    case ':w':
                        if (CURRENT_PROJECT_NAME) {
                            saveProject();
                        } else {
                            saveProjectAs();
                        }
                        break;
                    default:
                        // Check things that need regex
                        if (/:w \w+/.test(BUFFER)) {
                            // This will overwrite without prompt, just like vim (I think)
                            CURRENT_PROJECT_NAME = R.drop(3, BUFFER);
                            saveProject();
                        } else if (/:e \w+/.test(BUFFER)) {
                            openProject(R.drop(3, BUFFER));
                        } else if (/\/(\w|\\|\*|\/)+/.test(BUFFER)) {
                            // Make selected node first highlighted node
                            getHighlightedNode(R.add(1));
                            setMessage('');
                        } else {
                            // Unrecognized command
                            setActiveCommand(null);
                            setMessage(`Unrecognized command: ${BUFFER}`);
                        }
                }
                MODE = 'normal';
                ACTIVE_COMMAND = null;
                BUFFER = '';
            } else {
                setActiveCommand(null);
            }
            break;
        case 'b':
            if (MODE === 'normal') {
                NODE_POINTER--;
                if (NODE_POINTER < 0) NODE_POINTER = NODES.length - 1;
                setSelectedNode(NODES[NODE_POINTER]);
            }
            break;
        case '=':
            if (MODE === 'normal') {
                NODES = prettyPrintGraph(NODES, 1000);
            }
            break;
        case '|':
            if (MODE === 'normal') {
                alignChildren(SELECTED_NODE);
            }
            break;
    }

    if (MODE === 'insert' && SELECTED_NODE !== undefined) {
        if (evt.key.length === 1) {
            evt.preventDefault();
            if (SKIP_INSERT) {
                SKIP_INSERT = false;
            } else {
                SELECTED_NODE.setText(SELECTED_NODE.text + evt.key);
            }
        } else if (evt.key === 'Backspace') {
            evt.preventDefault();
            SELECTED_NODE.setText(R.init(SELECTED_NODE.text));
        } else if (evt.key === 'Enter') {
            SELECTED_NODE.setText(SELECTED_NODE.text + '\\n');
        }
        setMessage(SELECTED_NODE.text);
    } else if (MODE === 'buffer-text') {
        if (evt.key.length === 1) {
            if (R.contains(evt.key, ' /')) evt.preventDefault();
            BUFFER += evt.key;
            if (ACTIVE_COMMAND === '/') {
                searchGraph(R.drop(1, BUFFER));
            }
        } else if (evt.key === 'Backspace') {
            evt.preventDefault();
            BUFFER = R.init(BUFFER);
        }
        setMessage(BUFFER);
    }

    renderNodes(CANVAS, CTX);
}

function setSelectedNode(node) {
    console.assert(node, 'Selected node should never be undefined.')
    SELECTED_NODE = node;

    // Update the GUI to show whether the currently selected node is a Tree or Graph.
    document.getElementById('graph-type').innerText = isTree(SELECTED_NODE) ? 'Tree' : 'Graph';
}

function deleteReferencesTo(id) {
    NODES = NODES.map(n => {
        n.children = n.children.filter(c => c.id !== id);
        return n;
    });
}

function deleteNode(id) {
    NODES = NODES.filter(n => n.id !== id);
}

/**
 * Move the centre of the given node to the given point. Also move all other
 * nodes in the graph so that they're positions remain the same relative to the
 * given node.
 * @param {Number} x The x-axis coordinate to move the given node to
 * @param {Number} y The y-axis coordinate to move the given node to
 * @param {Node} node
 */
// FIXME: This function's signature/purpose is kind of confusing.
function moveGraphTo(x, y, node) {
    const diffX = node.x + Math.floor(node.width / 2) - x;
    const diffY = node.y + Math.floor(node.height / 2) - y;
    panGraph(-diffX, -diffY);
}

/**
 * Pan the graph.
 * @param {Number} x The amount to pan the graph horizontally by.
 * @param {Number} y The amount to pan the graph vertically by.
 */
function panGraph(x, y) {
    NODES.map(n => {
        n.x += x;
        n.y += y;
        return n;
    });
}

function renderNodes(canvas, context) {
    context.clearRect(0, 0, canvas.width, canvas.height);
    NODES.forEach(n => n.children.map(R.curry(drawLine)(context, n)));
    NODES.map(R.curry(renderNode)(context));
}

function renderNode(context, node) {
    const measurements = renderText(
        context,
        parseRichFormatting(node.text),
        node.x,
        node.y
    );
    node.width = measurements.width;
    node.height = measurements.height;

    if (node === SELECTED_NODE) {
        drawRectangleAroundNode(context, 'black', node);
        return;
    } else if (NODE_TO_CONNECT === node) {
        drawRectangleAroundNode(context, 'blue', node);
    } else if (node.highlight) {
        drawRectangleAroundNode(context, 'yellow', node);
    }
}

/**
 * Convert the format string into an object to be used by the `renderText` function
 * @param {String} str A string written in the rich format
 */
function parseRichFormatting(str) {
    let rtn = [];
    let acc = '';
    let inside = false;
    for (let i = 0; i < str.length; i++) {
        if (str[i] === '*' || str[i] === '/') {
            rtn.push({
                text: acc,
                style: !inside ? 'normal' : str[i] === '*' ? 'bold' : 'italic',
                break: false
            });
            acc = '';
            inside = !inside;
        } else if (str[i] === '\\' && i < str.length - 1) {
            if (str[i + 1] === 'n') {
                i++;
                rtn.push({ text: acc, style: 'normal', break: true });
                acc = '';
            } else if (str[i + 1] === '*' || str[i + 1] === '/') {
                acc += str[++i];
            } else {
                acc += str[i];
            }
        } else {
            acc += str[i];
        }
    }
    rtn.push({ text: acc, style: 'normal', break: false });
    return rtn.filter(R.prop('text'));
}

/**
 * Render text for a node
 * @param {Array<Object>} text An object representing rich text
 */
function renderText(context, text, x, y) {
    if (text.length === 0) {
        return { width: 0, height: 16 };
    }
    let heightOffset = 0;
    let widthOffset = 0;
    let maxWidth = context.measureText(text[0].text).width;
    for (let i = 0; i < text.length; i++) {
        switch (text[i].style) {
            case 'normal':
                context.font = '20px Helvetica';
                break;
            case 'bold':
                context.font = 'bold 20px Helvetica';
                break;
            case 'italic':
                context.font = 'italic 20px Helvetica';
                break;
            default:
                context.font = '20px Helvetica';
                break;
        }
        context.fillText(text[i].text, x + widthOffset, y + heightOffset);
        let textWidth = context.measureText(text[i].text).width;
        widthOffset += textWidth;
        if (widthOffset > maxWidth) maxWidth = widthOffset;
        if (text[i].break) {
            heightOffset += 20;
            widthOffset = 0;
        }
    }
    return { width: maxWidth, height: heightOffset + 16 };
}

function drawRectangleAroundNode(context, colour, node) {
    const previousStrokeStyle = context.strokeStyle;
    context.strokeStyle = colour;
    context.beginPath();
    context.rect(
        node.x - 10,
        node.y - 10 - 16,
        node.width + 20,
        node.height + 20
    );
    context.closePath();
    context.stroke();
    context.strokeStyle = previousStrokeStyle;
}

/**
 * Draws a line from one node to another
 * Instead of doing lots of math, we'll cheat and just draw the line
 * from the centre of one node to the center of the other, draw a white
 * rectangle over each node, then render each node.
 */
function drawLine(context, node1, node2) {
    // Draw the line
    context.beginPath();
    context.moveTo(
        node1.x + Math.floor(node1.width / 2),
        node1.y - 16 + Math.floor(node1.height / 2)
    );
    context.lineTo(
        node2.x + Math.floor(node2.width / 2),
        node2.y - 16 + Math.floor(node2.height / 2)
    );
    context.closePath();
    context.stroke();

    // Draw the white rectangles
    context.clearRect(
        node1.x - 10,
        node1.y - (16 + 10),
        node1.width + 20,
        node1.height + 20
    );
    context.clearRect(
        node2.x - 10,
        node2.y - (16 + 10),
        node2.width + 20,
        node2.height + 20
    );
}

function resetProjectState() {
    NODES = [new Node('Root', 0, 0)];
    NODE_POINTER = 0;
    setSelectedNode(NODES[0]);
    moveToCentreOfCanvas(NODES[0]);
    renderNodes(CANVAS, CTX);
}

function openProject(projName) {
    const name = projName || prompt('Name: ');
    let item = localStorage.getItem(name);
    if (!item) {
        newProject(name);
        return;
    }
    NODES = JSON.parse(localStorage.getItem(name || prompt('Name: '))).map(
        n => {
            const node = new Node(n.text, n.x, n.y, n.id);
            node.childIds = n.childIds;
            return node;
        }
    );
    for (let i = 0; i < NODES.length; i++) {
        while (NODES[i].childIds.length) {
            let childId = NODES[i].childIds.pop();
            NODES[i].connectTo(NODES.filter(n => n.id === childId)[0]);
        }
    }
    renderNodes(CANVAS, CTX);
    CURRENT_PROJECT_NAME = name;
    setMessage(`Opened project '${CURRENT_PROJECT_NAME}'`);
}

function saveProjectAs() {
    CURRENT_PROJECT_NAME = prompt('Save As: ');
    while (R.contains(CURRENT_PROJECT_NAME, EXISTING_PROJECTS)) {
        if (
            confirm(
                `Project '${CURRENT_PROJECT_NAME}' already exists. Do you want to overwrite it?`
            )
        ) {
            break;
        }
        CURRENT_PROJECT_NAME = prompt('Save As: ');
    }
    localStorage.setItem(
        CURRENT_PROJECT_NAME,
        JSON.stringify(
            NODES.map(node =>
                R.pick(
                    ['id', 'text', 'childIds', 'x', 'y'],
                    R.assoc('childIds', node.children.map(R.prop('id')), node)
                )
            )
        )
    );
    setMessage(`Saved project '${CURRENT_PROJECT_NAME}'`);
}

function saveProject() {
    if (!CURRENT_PROJECT_NAME) {
        throw Error('Tried to save a project with no name');
        return;
    }
    localStorage.setItem(
        CURRENT_PROJECT_NAME,
        JSON.stringify(
            NODES.map(node =>
                R.pick(
                    ['id', 'text', 'childIds', 'x', 'y'],
                    R.assoc('childIds', node.children.map(R.prop('id')), node)
                )
            )
        )
    );
    setMessage(`Saved project '${CURRENT_PROJECT_NAME}'`);
}

function newProject(projName) {
    CURRENT_PROJECT_NAME = projName || prompt('Name: ');
    resetProjectState();
    setMessage(`Started new project '${CURRENT_PROJECT_NAME}'`);
}

function genRandomId(n = 8) {
    let randomString = '';
    const alphabet =
        'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < n; i++) {
        randomString +=
            alphabet[Math.floor(Math.random() * (alphabet.length - 1))];
    }
    return randomString;
}

function setMessage(msg) {
    document.getElementById('message').innerText = msg;
}

function getExistingProjects() {
    return Object.keys(localStorage);
}

function setActiveCommand(command) {
    if (command) {
        setMessage(command);
        ACTIVE_COMMAND = command;
    } else {
        setMessage('');
        ACTIVE_COMMAND = null;
    }
}

/**
 * Search for nodes by looking for a substring in their content
 * @param {String} str The string to search for in node content
 */
function searchGraph(str) {
    if (str === '') return;
    NODES = NODES.map(n => {
        n.highlight = R.contains(str, removeFormattingCharacters(n.text));
        return n;
    });
}

function removeFormattingCharacters(str) {
    let rtn = '';

    // Replace all linebreaks with spaces
    str = str.replace('\\n', ' ');

    // Remove all '/'s and '*'s not preceded by a '\'
    let i = 0;
    while (i < str.length) {
        if (str[i] === '\\' && i < str.length - 1) {
            if (str[i + 1] === '*') {
                i++;
                rtn += '*';
            } else if (str[i + 1] === '/') {
                i++;
                rtn += '/';
            } else {
                rtn += '\\';
            }
        } else if (str[i] !== '*' && str[i] !== '/') {
            rtn += str[i];
        }
        i++;
    }
    return rtn;
}

function getHighlightedNode(fn) {
    NODE_POINTER = NODE_POINTER < 0 ? 0 : NODE_POINTER;
    const startingNode = NODES[NODE_POINTER];
    NODE_POINTER = (fn(NODE_POINTER) + NODES.length) % NODES.length;
    let nextNode = NODES[NODE_POINTER];
    while (nextNode !== startingNode) {
        if (nextNode.highlight) {
            setSelectedNode(nextNode);
            return;
        }
        NODE_POINTER = (fn(NODE_POINTER) + NODES.length) % NODES.length;
        nextNode = NODES[NODE_POINTER];
    }
    setSelectedNode(startingNode);
}

/**
 * Given a node, map a function over that node and all its descendants.
 * Modifies given nodes, does not create copies.
 * @param {Function(Node)} fn The function to map. Must modify the given node,
 * not return a modified copy.
 * @param {Node} node The root node
 */
function mapOverChildren(fn, node) {
    const seen = [];
    const f = node => {
        if (!R.contains(node.id, seen)) {
            fn(node);
            seen.push(node.id);
            node.children.map(f);
        }
    };
    f(node);
}

/**
 * Move a node by the amounts given
 * @param {Number} x Distance to move along the x-axis
 * @param {Number} y Distance to move along the y-axis
 * @param {Node} node The node to move
 * @returns {Node} The given node, but modified
 */
function moveNode(x, y, node) {
    node.x += x;
    node.y += y;
    return node;
}

function makeFullscreen() {
    document.body.classList.add('fullscreen');
    CANVAS.classList.remove('card');
    updateCanvasDimensions();
    renderNodes(CANVAS, CTX);
}

function restoreFromFullscreen() {
    document.body.classList.remove('fullscreen');
    CANVAS.classList.add('card');
    CANVAS.width = CANVAS_WIDTH;
    CANVAS.height = CANVAS_HEIGHT;
    renderNodes(CANVAS, CTX);
}

function updateCanvasDimensions() {
    const computedStyle = getComputedStyle(CANVAS);

    // Set the width and height properties of the canvas. For some reason
    // it only works if you put the value in a variable before assigning
    // it to CANVAS.height and CANVAS.width.
    const elementHeight =
        CANVAS.clientHeight -
        parseFloat(computedStyle.paddingTop) -
        parseFloat(computedStyle.paddingBottom);
    const elementWidth =
        CANVAS.clientWidth -
        parseFloat(computedStyle.paddingLeft) -
        parseFloat(computedStyle.paddingRight);
    CANVAS.height = elementHeight;
    CANVAS.width = elementWidth;
}

function exportToImage() {
    const xs = NODES.map(R.prop('x'));
    const ys = NODES.map(R.prop('y'));
    const minX = xs.reduce(R.min) - 10;
    const minY = ys.reduce(R.min) - 10 - 16;
    const maxX = NODES.map(n => n.x + n.width).reduce(R.max) + 10;
    const maxY = NODES.map(n => n.y + n.height - 16).reduce(R.max) + 10;
    const width = maxX - minX;
    const height = maxY - minY;
    const canvas = document.createElement('canvas');
    const context = canvas.getContext('2d');
    NODES.map(n => {
        n.x -= minX;
        n.y -= minY;
    });
    canvas.width = width;
    canvas.height = height;
    renderNodes(canvas, context);
    NODES.map(n => {
        n.x += minX;
        n.y += minY;
    });
    const dataUrl = canvas.toDataURL();
    const img = document.createElement('img');
    img.src = dataUrl;
    img.classList.add('card');
    img.id = 'export-img';

    if (!IS_EXPORT_VIEW) {
        document.body.classList.add('export-mode');
        document.getElementById('container').appendChild(img);
    } else {
        document.body.classList.remove('export-mode');
        document.getElementById('export-img').remove();
    }

    IS_EXPORT_VIEW = !IS_EXPORT_VIEW;
}

/**
 * Aligns the children of the given node in a horizontal line centered beneath
 * the given node.
 * @param {Node} node A node.
 */
function alignChildren(node) {
    // The middle of the node to align under
    const midX = node.x + node.width / 2;

    // Calculate the sum of the widths of the children, including gaps in between.
    const widthOfAllChildren =
        node.children.reduce((sum, child) => {
            return sum + child.width
        }, 0) +
        Math.max(0, node.children.length - 1) * GAP_SIZE;

    // Loop over children and line them up.
    let currentX = midX - widthOfAllChildren / 2;
    node.children.forEach((child, i, children) => {
        child.x = currentX;
        currentX += child.width + GAP_SIZE;
    });
}
