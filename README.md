# Modal Graph Editor

I find a lot of things are tree or graph structured. Examples include:

1.  Game situations and tactics like chess openings or Melee edgeguard flowcharts
2.  Arguments
3.  Learning new concepts

So, I'd like an editor that lets me create trees of information. Then I had the idea of making it controlled using commands like vim, which is a modal text editor.

Existing tree/graph editing tools I've used are Coggle and UMLet. I liked Coggle, but its features are limited for free users. In particular, you cannot make graphs without paying, only trees. It is also becomes slow very quickly. It has a killer feature that I love though, where it can basically "pretty-print" the entire tree at the click of a button. I'd like to implement that somehow in this project if I can. UMLet is better than Coggle for "node richness." By that I mean you can do a lot more formatting with each node in UMLet, whereas Coggle does mostly just text. Using UMLet feels very manual though.

So, Coggle falls short by not giving me graphs for free, and its performance also degrades fairly quickly. UMLet solves both of these problems, but is not as visually appealing, has a clunkier editing flow, and is an offline desktop app. Therefore this project will ideally meet the following requirements (roughly in order of importance):

1.  Ability to create trees and graphs
2.  Fluid workflow (accomplished through modal command system like vim)
3.  Performant
4.  Online
5.  Rich nodes
6.  Visually appealing
