/**
 * Pretty-prints a graph using a force-directed graph algorithm.
 * Based on the paper "A heuristic for graph drawing" by Peter Eades.
 * @param {Node[]} V An array of nodes.
 * @param {Integer} K The number of iterations to perform.
 * @returns {Node[]} The given array of nodes, but with modified x and y values.
 */
function prettyPrintGraph(V, K) {

  // Map the x and y values of the nodes to the center of the label rather than
  // top left corner. We revert this at the end of the function.
  V = V.map(v => {
    v.x += v.width / 2;
    v.y += v.height / 2;
    return v;
  });

  // Some constants to control how strong the forces are.
  const C1 = 2;
  const C2 = 100;
  const C3 = 1000;
  const C4 = 0.1;

  /// Calculates the force of repulsion between two nodes.
  const f_rep = (pu, pv) => {
    let a = vectorSubtract(pv, pu);
    let d = norm(a);
    return vectorMultByScalar(unit(a), C3 / Math.pow(d, 2));
  }

  /// Calculates the attractive force between two nodes.
  const f_spring = (pu, pv) => {
    let a = vectorSubtract(pu, pv);
    let d = norm(a);
    return vectorMultByScalar(unit(a), C1 * Math.log(d / C2));
  };

  let t = 0;
  while ( t < K ) {
    V = V.map(v => {
      let zeroVector = { x: 0, y: 0 };
      let F = vectorMultByScalar(
        vectorAdd(
          V.filter(u => u.id !== v.id && ! isConnectedTo(u, v)).reduce((f, u) => vectorAdd(f, f_rep(u, v)),    zeroVector),
          V.filter(u => u.id !== v.id &&   isConnectedTo(u, v)).reduce((f, u) => vectorAdd(f, f_spring(u, v)), zeroVector),
        ),
        C4
      );
      
      v.x += F.x;
      v.y += F.y;
      return v;
    });
    t++;
  }

  // Map the x and y values back to the top left corner.
  V = V.map(v => {
    v.x -= v.width / 2;
    v.y -= v.height / 2;
    return v;
  });

  return V;
}

function isConnectedTo(u, v) {
  return u.children.some(c => c.id === v.id) ||
         v.children.some(c => c.id === u.id);
}

function norm(v) {
  return Math.sqrt(Math.pow(v.x, 2) + Math.pow(v.y, 2));
}

function vectorAdd(u, v) {
  return {
    x: u.x + v.x,
    y: u.y + v.y
  };
}

function vectorSubtract(u, v) {
  return {
    x: u.x - v.x,
    y: u.y - v.y
  };
}

function vectorMultByScalar(v, scalar) {
  return {
    x: v.x * scalar,
    y: v.y * scalar
  };
}

function unit(v) {
  return vectorMultByScalar(v, 1 / norm(v));
}

/**
 * Determine if the given node is the root of a tree.
 * @param {Node} node A node.
 * @returns True if the node is the root of a tree, otherwise false.
 */
function isTree(node) {
  let seen = [];
  let queue = [node];

  while ( queue.length ) {
    let n = queue.pop();

    for ( let i = 0; i < n.children.length; i++ ) {
      if ( seen.includes(n.children[i].id) ) return false;
      seen.push(n.children[i].id);
      queue.push(n.children[i]);
    }
  }

  return true;
}
